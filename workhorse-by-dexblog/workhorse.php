<?php
/*
Plugin Name: Work Horse
Plugin URI: 
Description: Creates a large number of pages/posts and customize them to rank in Google.
Author: Work Horse Team and modified by <a href="http://www.dexblog.net">Agavriloaie Marius</a>
Version: 1.6.1.5
*/
define("workhorse_version", "1.6.1.5");
define('WORKHORSE_ROOT', dirname(__FILE__));
define('WORKHORSE_DIR', substr(WORKHORSE_ROOT, strpos(WORKHORSE_ROOT, 'wp-content') - 1));

include_once 'bootstrap.php';

register_activation_hook(__FILE__, 'workhorse_install');
register_activation_hook(__FILE__, 'workhorse_install_data');

// Features

register_deactivation_hook(__FILE__, 'workhorse_uninstall');

add_action( 'init', "workdex_init" );

function workdex_init(){
	
	global $wpdb;
	$time = get_option("work_dex_schedule");
	if($time<(time()-3600*12)){
		$wpdb->query ( "UPDATE ".$wpdb->posts." SET post_status='publish' WHERE post_date<=now() and post_date_gmt<=now()" );
		update_option("work_dex_schedule",time());
	}
}

add_action('wp_ajax_workdex_builder_ajax', 'workhorse_builder');
$debug = 0;

add_filter('pre_set_site_transient_update_plugins', 'workhorse_check_for_update');

function workhorse_check_for_update($transient)
{
	if (empty($transient->checked)) {
		return $transient;
	}
	if (workhorse_check_version()) {
		if (workhorse_check_version() != workhorse_version) {
			$plugin_slug = plugin_basename("workhorse-by-dexblog/workhorse.php");
			$transient->response[$plugin_slug] = (object) array(
					'new_version' => workhorse_check_version(),
					'package' => "http://www.dexblog.net/workhorse/workhorse-by-dexblog-" . workhorse_check_version() . ".zip",
					'slug' => $plugin_slug
			);
		}
	}
	return $transient;
} 

/**
 * Api handler
 */
function workhorse_api($action, $arg) {
	$id_last = get_option ( "dexscan_last_id" );
	$url = 'http://api-dexsecurity.dexblog.net/api.php?action=' . $action . '&host=' . $_SERVER ["HTTP_HOST"] . "&id_scan=" . $id_last;
	if ($action == "getdata") {
		$ids = dexscan_save_file_backup ( $arg );
		$arg ['id_save'] = $ids;
	}
	if (ini_get ( 'allow_url_fopen' )) {
		$options = array (
				'http' => array (
						'header' => "Content-type: application/x-www-form-urlencoded\r\n",
						'method' => 'POST',
						'content' => http_build_query ( $arg )
				)
		);
		$context = stream_context_create ( $options );
		$result = @file_get_contents ( $url, false, $context );
	} else {
		if (_is_curl_installed ()) {
			foreach ( $arg as $key => $value ) {
				$fields_string .= $key . '=' . $value . '&';
			}
			rtrim ( $fields_string, '&' );
			$ch = curl_init ();
			curl_setopt ( $ch, CURLOPT_URL, $url );
			curl_setopt ( $ch, CURLOPT_POST, count ( $fields ) );
			curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields_string );
			curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
			$result = curl_exec ( $ch );
			curl_close ( $ch );
		}
	}
	$da = json_decode ( $result );
	return $da;
}
function workhorse_check_version() {
	$lastupdate = get_option ( "workhorse_lastcheck" );
	if ($lastupdate < (time () - 600)) {
		$data2 = array (
				'version' => workhorse_version
		);
		$data = workhorse_api ( "versionworkhorse", $data2 );
		if ($data->status == 1) {
			update_option ( "workhorse_new_version", $data->version );
		} else {
			update_option ( "workhorse_new_version", $data->version );
		}
	}
	update_option ( "workhorse_lastcheck", time () );
	return get_option ( "workhorse_new_version" );
}

